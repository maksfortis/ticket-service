<?php


namespace App\Repository\Intefaces;


use App\Http\Requests\StoreTicketRequest;
use Illuminate\Http\Request;

interface TicketsInterface
{

    /**
     * @param int $page
     * @param int $limit
     *
     */
    public function getTickets(int $page, int $limit);

    /**
     * @param string $ticketId
     *
     */
    public function getTicket(string $ticketId): object;

    /**
     * @param StoreTicketRequest $request
     *
     */
    public function storeTicket(StoreTicketRequest $request): object;

    /**
     * @param string $status
     * @param string $ticketId
     *
     * @return mixed
     */
    public function updateTicket(string $status, string $ticketId): bool;

    /**
     * @param Request $request
     * @param string  $ticketId
     *
     * @return bool
     */
    public function addUser(Request $request, string $ticketId): bool;

}
