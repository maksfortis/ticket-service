<?php


namespace App\Repository\Intefaces;


interface MessagesInterface
{
    /**
     * @param string $ticketId
     *
     * @return array
     */
    public function getMessagesByTicket(string $ticketId): array;

    /**
     * @param        $request
     * @param string $ticketId
     *
     * @return bool
     */
    public function storeMessage($request, string $ticketId): bool;

}
