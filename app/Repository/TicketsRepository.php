<?php


namespace App\Repository;

use App\Entities\Ticket;
use App\Http\Requests\StoreTicketRequest;
use App\Repository\Intefaces\TicketsInterface;
use App\Services\ResponseBuilder\Builders\ResponseBuilder;
use App\Services\TicketPopulate\TicketPopulate;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Mockery\Exception;


class TicketsRepository implements TicketsInterface
{

    protected ResponseBuilder $builder;
    protected TicketPopulate  $ticketPopulate;

    /**
     * TicketsRepository constructor.
     *
     * @param ResponseBuilder $builderResponse
     */
    public function __construct(ResponseBuilder $builderResponse, TicketPopulate $ticketPopulate)
    {
        $this->builder        = $builderResponse;
        $this->ticketPopulate = $ticketPopulate;

    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return Collection
     */
    public function getTickets(int $page, int $limit): Collection
    {
        $offset = ($page - 1) * $limit;
        if (!empty(request()->get('user'))) {
            $tickets = DB::table('tickets')
                ->join('ticket_users', function ($join) {
                    $join->on('tickets.id', '=', 'ticket_users.ticket_id')
                        ->where('ticket_users.user_id', '=', request()->get('user'));
                })
                ->offset($offset)
                ->limit($limit)
                ->get();
        } else {
            $tickets = DB::table('tickets')->limit($limit)->get();
        }
        foreach ($tickets as $ticket) {
            $result[] = $this->ticketPopulate->populate((array) $ticket);
        }

        return collect($result);
    }

    /**
     * @param string $ticketId
     *
     * @return object
     */
    public function getTicket(string $ticketId): object
    {
        return DB::table('tickets')->find($ticketId);
    }

    /**
     * @param StoreTicketRequest $request
     *
     * @return object
     */
    public function storeTicket(StoreTicketRequest $request): object
    {
        $ticketId = Str::uuid();

        DB::table('tickets')->insert([
            'id'   => $ticketId,
            'name' => $request['name'],
        ]);

        DB::table('ticket_users')->insert([
            'id'        => Str::uuid(),
            'ticket_id' => $ticketId,
            'user_id'   => $request['userId'],
            'user_type' => Ticket::USER_TYPE_CLIENT,
        ]);

        return DB::table('tickets')->find($ticketId);
    }

    /**
     * @param string $status
     * @param string $ticketId
     *
     * @return bool
     */
    public function updateTicket(string $status, string $ticketId): bool
    {
        try {
            DB::table('tickets')->where('id', $ticketId)->update(['status' => $status]);

            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * @param        $request
     * @param string $ticketId
     *
     * @return bool
     */
    public function addUser($request, string $ticketId): bool
    {
        try {
            DB::table('ticket_users')->insert([
                'id'        => Str::uuid(),
                'ticket_id' => $ticketId,
                'user_id'   => $request['userId'],
                'user_type' => self::USER_TYPE_STAFF,
            ]);

            return true;
        } catch (Exception $exception) {
            return false;
        }
    }
}
