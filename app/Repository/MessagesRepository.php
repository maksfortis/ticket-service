<?php


namespace App\Repository;


use App\Repository\Intefaces\MessagesInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Mockery\Exception;

class MessagesRepository implements MessagesInterface
{

    /**
     * @param string $ticketId
     *
     * @return array
     */
    public function getMessagesByTicket(string $ticketId): array
    {
        $messages = DB::table('messages')
            ->where('ticket_id', $ticketId)
            ->get()
            ->toArray();

        foreach ($messages as $message) {
            unset($message->ticket_id);
        }

        return $messages;
    }

    /**
     * @param        $request
     * @param string $ticketId
     *
     * @return bool
     */
    public function storeMessage($request, string $ticketId): bool
    {
        try {
            return DB::table('messages')->insert([
                'id'        => Str::uuid(),
                'ticket_id' => $ticketId,
                'user_id'   => $request['userId'],
                'text'      => $request['text'],
            ]);
        } catch (Exception $exception) {
            return false;
        }

    }
}
