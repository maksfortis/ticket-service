<?php


namespace App\Entities;

use App\Services\ResponseBuilder\Entities\Entity;
use DateTimeInterface;

class Ticket implements Entity
{

    private string $id;
    private string $name;
    private string $description;

    /**
     * Ticket constructor.
     *
     * @param string $id
     * @param string $name
     * @param string $description
     */
    public function __construct(
        string $id,
        string $name,
        string $description,
    )
    {
        $this->id          = $id;
        $this->name        = $name;
        $this->description = $description;
    }

    public function getUser()
    {
        //
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getCreatedAt()
    {
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name'=> $this->name
        ];
    }
}
