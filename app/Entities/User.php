<?php


namespace App\Entities;


use App\Services\ResponseBuilder\Entities\Entity;

class User  implements Entity
{
    public const USER_TYPE_CLIENT = 'client';
    public const USER_TYPE_STAFF  = 'staff';

    private string $id;
    private string $type;

    public function __construct(string $id, string $type)
    {
        $this->id   = $id;
        $this->type = $type;
    }

    public function toArray(): array
    {
        // TODO: Implement toArray() method.
    }
}
