<?php


namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\DatabaseRule;

class DBExists implements ImplicitRule
{
    use DatabaseRule;

    public function __construct($table, $column = 'NULL')
    {
        $this->table  = $table;
        $this->column = $column;
    }

    /**
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return DB::table($this->table)->where($this->column ?? $attribute, $value)->exists();
    }

    public function message()
    {
        return 'The :attribute cannot be an empty string.';
    }


}
