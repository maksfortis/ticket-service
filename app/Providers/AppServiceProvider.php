<?php

namespace App\Providers;

use App\Services\LocaleChecker\AppLocaleChecker;
use App\Services\LocaleChecker\Interfaces\LocaleChecker;
use App\Services\ResponseBuilder\HeadersFactory\Base\HeadersFactoryInterface;
use App\Services\ResponseBuilder\HeadersFactory\HeadersFactory;
use App\Services\ResponseBuilder\SerializerFactory\Base\SerializerFactoryInterface;
use App\Services\ResponseBuilder\SerializerFactory\SerializerFactory;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use GuzzleHttp\ClientInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(LocaleChecker::class, AppLocaleChecker::class);
        $this->app->bind(ClientInterface::class, Client::class);
        $this->app->bind(SerializerFactoryInterface::class, SerializerFactory::class);
        $this->app->bind(HeadersFactoryInterface::class, HeadersFactory::class);
    }
}
