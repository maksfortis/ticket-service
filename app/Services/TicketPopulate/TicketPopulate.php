<?php


namespace App\Services\TicketPopulate;

use App\Entities\Ticket;
use Carbon\Carbon;

class TicketPopulate
{

    /**
     * @param array $data
     *
     * @return Ticket
     */
    public function populate(array $data): Ticket
    {
        return new Ticket(
            $data['id'],
            $data['name'],
            $data['description'],
        );
    }

}
