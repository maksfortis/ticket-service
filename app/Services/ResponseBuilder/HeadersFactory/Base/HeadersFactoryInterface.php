<?php

namespace App\Services\ResponseBuilder\HeadersFactory\Base;

/**
 * Interface HeadersFactoryInterface
 * @package App\HeadersFactory
 */
interface HeadersFactoryInterface
{
    /**
     * @param string $responseType
     *
     * @return array
     */
    public function create(string $responseType): array;
}
