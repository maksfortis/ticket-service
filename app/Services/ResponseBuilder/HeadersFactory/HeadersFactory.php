<?php

namespace App\Services\ResponseBuilder\HeadersFactory;

use App\Services\ResponseBuilder\HeadersFactory\Base\HeadersFactoryInterface;

/**
 * Class HeadersFactory
 * @package App\HeadersFactory
 */
class HeadersFactory implements HeadersFactoryInterface
{
    public const XML_RESPONSE  = 'xml';
    public const JSON_RESPONSE = 'json';

    /**
     * @inheritdoc
     */
    public function create(string $responseType): array
    {
        if ($responseType === static::JSON_RESPONSE) {
            $headers = ['Content-Type' => 'application/json'];
        } elseif ($responseType === static::XML_RESPONSE) {
            $headers = ['Content-Type' => 'application/xml'];
        } else {
            $headers = [];
        }

        return $headers;
    }
}
