<?php

namespace App\Services\ResponseBuilder\Exceptions;

/**
 * Class UnknownSerializerType
 * @package App\Exceptions
 */
class UnknownSerializerType extends \Exception
{

}
