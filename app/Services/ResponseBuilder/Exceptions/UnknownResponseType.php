<?php

namespace App\Services\ResponseBuilder\Exceptions;

/**
 * Class UnknownResponseType
 * @package App\Exceptions
 */
class UnknownResponseType extends \Exception
{

}
