<?php

namespace App\Services\ResponseBuilder\SerializerFactory;

use App\Services\ResponseBuilder\Exceptions\UnknownSerializerType;
use App\Services\ResponseBuilder\SerializerFactory\Base\SerializerFactoryInterface;
use App\Services\ResponseBuilder\Serializers\Serializer;

/**
 * Class SerializerFactory
 * @package App\SerializerFactory
 */
class SerializerFactory implements SerializerFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(string $type): Serializer
    {
        $serializerName = '\App\Services\ResponseBuilder\Serializers\\' . ucfirst($type) . 'Serializer';

        if (class_exists($serializerName)) {
            return new $serializerName();
        }

        throw new UnknownSerializerType("Type '$type' cannot be used");
    }
}
