<?php

namespace App\Services\ResponseBuilder\SerializerFactory\Base;

use App\Services\ResponseBuilder\Exceptions\UnknownSerializerType;
use App\Services\ResponseBuilder\Serializers\Serializer;

/**
 * Interface SerializerFactoryInterface
 * @package App\SerializerFactory
 */
interface SerializerFactoryInterface
{
    /**
     * @param string $type
     *
     * @return Serializer
     * @throws UnknownSerializerType
     */
    public function create(string $type): Serializer;
}
