<?php

namespace App\Services\ResponseBuilder\Serializers;

use Spatie\ArrayToXml\ArrayToXml;

class XmlSerializer implements Serializer
{
    /**
     * @param array $data
     *
     * @return string
     */
    public function serialize(array $data): string
    {
        return ArrayToXml::convert($data);
    }
}
