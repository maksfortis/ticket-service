<?php

namespace App\Services\ResponseBuilder\Entities;

/**
 * Interface Entity
 * @package App\Entities
 */
interface Entity
{
    public function toArray(): array;
}
