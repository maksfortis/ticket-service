<?php

namespace App\Services\LocaleChecker\Interfaces;

use App\Services\LocaleChecker\Exceptions\UnknownLocale;

/**
 * Interface LocaleChecker
 * @package App\Services\LocaleChecker
 */
interface LocaleChecker
{
    /**
     * @param string $locale
     *
     * @throws UnknownLocale
     */
    public function check(string $locale): void;
}
