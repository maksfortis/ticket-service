<?php

namespace App\Services\LocaleChecker\Exceptions;

use Exception;

/**
 * Class UnknownLocale
 * @package App\Services\LocaleChecker\Exceptions
 */
class UnknownLocale extends Exception
{

}
