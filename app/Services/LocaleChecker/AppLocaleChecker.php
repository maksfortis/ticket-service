<?php

namespace App\Services\LocaleChecker;

use App\Services\LocaleChecker\Exceptions\UnknownLocale;
use App\Services\LocaleChecker\Interfaces\LocaleChecker;

/**
 * Class AppLocaleChecker
 * @package App\Services\LocaleChecker
 */
class AppLocaleChecker implements LocaleChecker
{
    /**
     * @inheritDoc
     */
    public function check(string $locale): void
    {
        if (!in_array($locale, config('app.available_locales'), true)) {
            throw new UnknownLocale();
        }
    }
}
