<?php


namespace App\Services\Selectel\CloudStorage\Interfaces;


interface SelectelCloudStorage
{
    public function getFiles(string $ticketId);

    public function storeFiles($file, string $ticketId);

}
