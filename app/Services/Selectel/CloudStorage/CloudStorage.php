<?php
//
//
//namespace App\Services\Selectel\CloudStorage;
//
//
//use App\Services\Selectel\CloudStorage\Interfaces\SelectelCloudStorage;
//use GuzzleHttp\Client;
//use GuzzleHttp\Exception\GuzzleException;
//use Psr\Http\Message\StreamInterface;
//
//class CloudStorage implements SelectelCloudStorage
//{
//    private Auth   $auth;
//    private Client $client;
//    private string $pathToContainer;
//    private string $tokenNameHeader;
//
//    /**
//     * CloudStorage constructor.
//     *
//     * @param Auth   $auth
//     * @param Client $client
//     */
//    public function __construct(Auth $auth, Client $client)
//    {
//        $this->auth            = $auth;
//        $this->client          = $client;
//        $this->pathToContainer = env('SELECTEL_PROJECT_URL') . '/' . env('SELECTEL_PROJECT_NAME') . '/';
//        $this->tokenNameHeader = 'X-Auth-Token';
//    }
//
//    /**
//     * @param string $ticketId
//     *
//     * @return StreamInterface
//     * @throws GuzzleException
//     */
//    public function getFiles(string $ticketId)
//    {
//        $files = $this->client->get($this->pathToContainer . $ticketId, [
//            'headers' => [
//                $this->tokenNameHeader => $this->auth->getToken(),
//            ],
//        ])->getBody();
//
//        return $files;
//    }
//
//    /**
//     * @param        $file
//     * @param string $ticketId
//     */
//    public function storeFiles($file, string $ticketId)
//    {
//        try {
//            $this->client->put($this->pathToContainer . 'file_name.jpg', [
//                'headers' => [
//                    'X-Auth-Token' => $this->auth->getToken(),
//                ],
//            ]);
//        } catch (GuzzleException $e) {
//        }
//
//    }
//}
