<?php
//
//namespace App\Services\Selectel\CloudStorage;
//
//use App\Services\Selectel\CloudStorage\Exceptions\invalidToken;
//use GuzzleHttp\Client;
//use GuzzleHttp\ClientInterface;
//use GuzzleHttp\Exception\ClientException;
//use GuzzleHttp\Exception\GuzzleException;
//use Illuminate\Support\Facades\DB;
//
//class Auth
//{
//    private array           $auth;
//    private string          $tokenNameHeader;
//    private ClientInterface $client;
//
//    /**
//     * Auth constructor.
//     */
//    public function __construct(Client $client)
//    {
//        $this->auth            = [
//            'X-Auth-User' => env('SELECTEL_USER'),
//            'X-Auth-Key'  => env('SELECTEL_PASSWORD'),
//        ];
//        $this->tokenNameHeader = 'X-Auth-Token';
//        $this->client          = $client;
//    }
//
//    /**
//     * @throws GuzzleException
//     */
//    public function getToken()
//    {
//        try {
//            $token = DB::table('setting')->where('key', $this->tokenNameHeader)->value('value');
//            $this->client->request('get', env('SELECTEL_PROJECT_URL'), [
//                'headers' => [
//                    $this->tokenNameHeader => $token,
//                ],
//            ]);
//
//            return $token;
//        } catch (invalidToken | ClientException $exception) {
//            $response = $this->client->request('get', env('SELECTEL_AUTH_URL'), [
//                'headers' => $this->auth,
//            ]);
//            $this->storeToken($response->getHeaderLine($this->tokenNameHeader));
//
//            return $response->getHeaderLine($this->tokenNameHeader);
//        }
//    }
//
//    private function storeToken(string $token): void
//    {
//        DB::table('setting')
//            ->where('key', $this->tokenNameHeader)
//            ->update([
//                'value' => $token,
//            ]);
//    }
//}
