<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddUserRequest;
use App\Http\Requests\StoreTicketRequest;
use App\Http\Requests\UpdateTicketRequest;
use App\Services\ResponseBuilder\Builders\ResponseBuilder;
use App\Services\ResponseBuilder\Entities\EntitiesMetaData;
use App\Services\ResponseBuilder\Entities\EntityCollection;
use Illuminate\Http\JsonResponse;
use App\Repository\Intefaces\TicketsInterface;
use Symfony\Component\HttpFoundation\Response;

class TicketsController extends Controller
{
    private const DEFAULT_PAGE_LIMIT  = 25;
    private const DEFAULT_PAGE_NUMBER = 1;

    private int $pageTotalCount;
    private int $totalCount;

    protected TicketsInterface $ticket;
    protected ResponseBuilder  $builder;

    /**
     * TicketsController constructor.
     *
     * @param TicketsInterface $ticketsInterfaceRepository
     * @param ResponseBuilder  $builderResponse
     */
    public function __construct(TicketsInterface $ticketsInterfaceRepository, ResponseBuilder $builderResponse)
    {
        $this->ticket  = $ticketsInterfaceRepository;
        $this->builder = $builderResponse;
    }

    /**
     *
     * @return JsonResponse
     */
    public function index(): Response
    {
        $limit   = \request()->get('limit') ?? self::DEFAULT_PAGE_LIMIT;
        $page    = \request()->get('page') ?? self::DEFAULT_PAGE_NUMBER;
        $tickets = $this->ticket->getTickets($page, $limit);
//        $data    = $this->builder->getResponseSuccess(Response::HTTP_OK, __('messages.tickets.get'), $tickets);

        $collection = new EntityCollection();

        foreach ($tickets as $ticket) {
            $collection->addEntity($ticket);
        }

        return $this->builder->createEntitiesResponse(Response::HTTP_OK, __('messages.tickets.get'), $collection, new EntitiesMetaData(1, 1, 1, 1));

//        return response()->json($data);
    }
//
//
    /**
     * @param $ticketId
     *
     * @return JsonResponse
     */
    public function showTicket($ticketId): Response
    {
        $ticket = $this->ticket->getTicket($ticketId);

        return $this->builder->createEntityResponse($this->ticket->getTicket($ticketId),Response::HTTP_OK, __('messages.tickets.get'));

    }
//
//    /**
//     * @param StoreTicketRequest $request
//     *
//     * @return JsonResponse
//     */
//    public function storeTicket(StoreTicketRequest $request): JsonResponse
//    {
//
//        $ticket = $this->ticket->storeTicket($request);
////        $data   = $this->builder->getResponseSuccess(Response::HTTP_OK, __('messages.tickets.create'), $ticket);
//
//        return response()->json($data);
//    }
//
//    /**
//     * @param UpdateTicketRequest $request
//     * @param                     $ticketId
//     *
//     * @return JsonResponse
//     */
//    public function updateTicket(UpdateTicketRequest $request, $ticketId): JsonResponse
//    {
//        $this->ticket->updateTicket($request['status'], $ticketId);
////        $data = $this->builder->getResponseSuccess(Response::HTTP_OK, __('messages.tickets.update'));
//
//        return response()->json($data);
//    }
//
//    /**
//     * @param AddUserRequest $request
//     * @param                $ticketId
//     *
//     * @return JsonResponse
//     */
//    public function addUser(AddUserRequest $request, $ticketId): JsonResponse
//    {
//
//        $this->ticket->addUser($request, $ticketId);
////        $data = $this->builder->getResponseSuccess(Response::HTTP_OK, __('messages.user.added'));
//
//        return response()->json($data);
//    }

}
