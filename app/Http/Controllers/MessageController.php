<?php


namespace App\Http\Controllers;


use App\Http\Requests\StoreMessageRequest;
use App\Repository\Intefaces\MessagesInterface;
use App\Services\ResponseBuilder\Builders\ResponseBuilder;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends Controller
{
    protected ResponseBuilder   $builder;
    protected MessagesInterface $messages;

    /**
     * MessageController constructor.
     *
     * @param ResponseBuilder   $builderResponse
     * @param MessagesInterface $messagesInterfaceRepository
     */
    public function __construct(ResponseBuilder $builderResponse, MessagesInterface $messagesInterfaceRepository)
    {
        $this->builder  = $builderResponse;
        $this->messages = $messagesInterfaceRepository;
    }

    /**
     * @param $ticketId
     *
     * @return JsonResponse
     */
    public function index($ticketId): JsonResponse
    {
        $messages = $this->messages->getMessagesByTicket($ticketId);
        $data     = $this->builder->getResponseSuccess(Response::HTTP_OK, __('messages.message.get'), $messages);

        return response()->json($data);
    }

    /**
     * @param StoreMessageRequest $request
     * @param                     $ticketId
     *
     * @return JsonResponse
     */
    public function storeMessage(StoreMessageRequest $request, $ticketId): JsonResponse
    {

        $message = $this->messages->storeMessage($request, $ticketId);
        $data    = $this->builder->getResponseSuccess(Response::HTTP_OK, __('messages.message.create'), $message);

        return response()->json($data);
    }
}
