<?php

namespace App\Http\Middleware;

use App\Services\LocaleChecker\Exceptions\UnknownLocale;
use App\Services\LocaleChecker\Interfaces\LocaleChecker;
use Closure;
use Illuminate\Support\Facades\App;
use Laravel\Lumen\Http\Request;

/**
 * Class Localize
 * @package App\Http\Middleware
 */
class Localize
{
    private LocaleChecker $localeChecker;

    /**
     * Localize constructor.
     *
     * @param LocaleChecker $localeChecker
     */
    public function __construct(LocaleChecker $localeChecker)
    {
        $this->localeChecker = $localeChecker;
    }

    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     * @throws UnknownLocale
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $locale = $request->get('locale') ?? config('app.locale');

        $this->localeChecker->check($locale);

        App::setLocale($locale);

        return $next($request);
    }
}
