<?php

namespace App\Http\Requests;

use App\Rules\DBExists;
use Pearl\RequestValidate\RequestAbstract;

class AddUserRequest extends RequestAbstract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'userId'   => ['required', 'string'],
            'ticketId' => ['required', 'string', new DBExists('tickets', 'id')],
        ];
    }

    public function all($keys = null): array
    {
        return array_merge(parent::all(), ['ticketId' => $this->route('ticketId')]);
    }
}
