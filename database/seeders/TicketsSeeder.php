<?php

namespace Database\Seeders;

use App\Models\Message;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Ticket;

class TicketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tickets = [
            [
                'name'        => 'Название первого тикета',
                'description' => 'Описание первого тикета',
                'status'      => Ticket::TICKET_NEW,
            ],
            [
                'name'        => 'Название второго тикета',
                'description' => 'Описание второго тикета',
                'status'      => Ticket::TICKET_WAITING_RESPONSE_STAFF,
            ],
            [
                'name'        => 'Название третьего тикета',
                'description' => 'Описание третьего тикета',
                'status'      => Ticket::TICKET_CLOSED,
            ],
        ];

        $messages = [
            [
                'text'   => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            ],
            [
                'text'   => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            ],
            [
                'text'   => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            ],
            [
                'text'   => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            ],
        ];

        foreach ($tickets as $ticket) {
            $ticketId = Str::uuid();
            $ticket = DB::table('tickets')->insert([
                'id'          => $ticketId,
                'name'        => $ticket['name'],
                'description' => $ticket['description'],
                'status'      => $ticket['status'],
            ]);
            foreach ($messages as $message) {
                DB::table('messages')->insert([
                    'id'        => Str::uuid(),
                    'ticket_id' => $ticketId,
                    'user_id'   => Str::uuid(),
                    'text'      => $message['text'],
                ]);
            }
        }
    }
}
