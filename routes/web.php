<?php

/** @var Router $router */

use App\Services\Selectel\CloudStorage\Auth;
use Laravel\Lumen\Routing\Router;
use function OpenApi\scan;

$router->group([
    'prefix' => 'tickets',
], function () use ($router) {

    $router->get('/', 'TicketsController@index');
    $router->get('/{ticketId}', 'TicketsController@showTicket');

    $router->post('/', 'TicketsController@storeTicket');
    $router->put('/{ticketId}', 'TicketsController@updateTicket');
    $router->post('/{ticketId}/user', 'TicketsController@addUser');

    $router->get('/{ticketId}/messages', 'MessageController@index');
    $router->post('/{ticketId}/message', 'MessageController@storeMessage');
});

$router->get('/doc', function () {
    return view('doc');
});

$router->get('doc-json', function () {
    return scan(base_path('app'))->toJson();
});
