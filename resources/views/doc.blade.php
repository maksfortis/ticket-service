<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Analytics.place Swagger</title>

    <link rel="stylesheet" href="/assets/swagger-style.css">

    <style>
        html {
            box-sizing: border-box;
        }

        *, *:before, *:after {
            box-sizing: inherit;
        }

        body {
            margin: 0;
            background: #fafafa;
        }
    </style>
</head>
<body>
<div id="swagger-ui"></div>

<script src="/assets/swagger-bundle.js"></script>

<script>
    window.onload = function () {
        window.ui = SwaggerUIBundle({
            url: '{{env('APP_URL')}}/doc-json',
            dom_id: '#swagger-ui',
            deepLinking: true,
            presets: [
                SwaggerUIBundle.presets.apis,
            ],
            layout: 'BaseLayout',
        });};
</script>
</body>
</html>
