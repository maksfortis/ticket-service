<?php

return [
    'tickets' => [
        'get'    => 'Tickets are loaded',
        'create' => 'The ticket was created successfully',
        'update' => 'The ticket was successfully updated',
    ],
    'message' => [
        'get'    => 'The messages are loaded',
        'create' => 'The message was created successfully',
    ],
    'user'    => [
        'added' => 'User added',
    ],
];
